import { boot } from 'quasar/wrappers';
import { Notify } from 'quasar';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default boot(({ Vue }) => {
  Notify.setDefaults({
    position: 'top',
    timeout: 2500,
    textColor: 'white',
    actions: [{ icon: 'close', color: 'white' }]
  });

  Vue.prototype.$q.notify.error = message => {
    Notify.create({
      message,
      color: 'negative'
    });
  };

  Vue.prototype.$q.notify.success = message => {
    Notify.create({
      message,
      color: 'positive'
    });
  };

  Vue.prototype.$q.notify.responseDataError = message => {
    return {
      response: {
        data: {
          error: message
        }
      }
    };
  };
});
