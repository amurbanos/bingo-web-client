import axios, { AxiosInstance } from 'axios'
import { boot } from 'quasar/wrappers'
import { Loading, LocalStorage } from 'quasar'
declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosInstance
  }
}

export default boot(({ Vue }) => {
  const token = LocalStorage.getItem('token')
  if (token) {
    axios.defaults.headers.post['Authorization'] = `Bearer ${token}`
    axios.defaults.headers.get['Authorization'] = `Bearer ${token}`
    axios.defaults.headers.put['Authorization'] = `Bearer ${token}`
  }

  axios.defaults.headers.post['Accept'] = 'application/json'
  axios.defaults.headers.get['Accept'] = 'application/json'
  axios.defaults.headers.put['Accept'] = 'application/json'

  axios.interceptors.request.use(config => {
    Loading.show({
      delay: 400
    })
    return config
  })

  axios.defaults.transformResponse = data => {
    Loading.hide()
    return JSON.parse(data)
  }

  Vue.prototype.$axios = axios.create({
    baseURL: `${process.env.API}/api1`
  })
})
