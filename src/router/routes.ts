import { RouteConfig } from 'vue-router'
import { Notify, LocalStorage } from 'quasar'

const isAuthenticated = () => {
  const token = LocalStorage.getItem('token')

  if (!token) {
    return false
  }

  // Request para endpoint que informa se usuário está autenticado (token é válido)

  return true
}
const routes: RouteConfig[] = [
  {
    // Rotas autenticadas
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: (to, from, next) => {
      if (!isAuthenticated()) {
        Notify.create({
          message: 'Você não tem autorização para acessar esta página',
          color: 'negative'
        })
        LocalStorage.remove('token')
        next({ name: 'login' })
      }
      next()
    },
    children: [
      {
        name: 'admin',
        path: '',
        component: () => import('pages/Home.vue')
      },
      {
        name: 'tv',
        path: 'tv',
        component: () => import('pages/Tv.vue')
      },
      {
        name: 'minha-conta',
        path: 'minha-conta',
        component: () => import('pages/MinhaConta.vue')
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: (to, from, next) => {
      if (isAuthenticated()) {
        next({ name: 'admin' })
      }
      next()
    },
    children: [
      {
        name: 'login',
        path: 'login',
        component: () => import('pages/Login.vue')
      },
      { path: 'cadastro', component: () => import('pages/Cadastro.vue') },
      {
        path: 'recuperar-senha',
        component: () => import('pages/RecuperarSenha.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
